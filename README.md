Server Tool
===

Use shell script to install the tools I used very often, all tested on the OS below:

3. Ubuntu 16
4. Ubuntu 20
5. Ubuntu 21
6. CentOS 7
7. CentOS 8
8. AlmaLinux 8.5
9. Rocky Linux 8.5

```text
AlmaLinux 8.5 can't install neovim
```

### [neovim](https://github.com/neovim/neovim)

Great command line editor.

```bash
bash neovim.sh
```

### [tmux](https://github.com/tmux/tmux)

Screen split tool, should execute `Ctrl + b` and `I` after install to load the theme.

```bash
bash tmux.sh
```

### [tig](https://github.com/jonas/tig)

Tig is a ncurses-based text-mode interface for git

```bash
bash tig.sh
```

### [jq](https://github.com/stedolan/jq)

jq is a lightweight and flexible command-line JSON processor.

```bash
bash jq.sh
```

### [jc](https://github.com/kellyjonbrazil/jc)

jc JSONifies the output of many CLI tools and file-types for easier parsing in scripts. See the Parsers section for
supported commands and file-types.

```bash
bash jc.sh
```

### [yq](https://github.com/mikefarah/yq)

a lightweight and portable command-line YAML, JSON and XML processor. yq uses jq like syntax but works with yaml files as well as json and xml. It doesn't yet support everything jq does - but it does support the most common operations and functions, and more is being added continuously.

```bash
bash yq.sh
```
