#!/bin/bash

base_path="$(dirname "$0")"

. "$base_path/base.sh"

go_ubuntu() {
  echo -e Install tig on "$(yellow_color Ubuntu)"

  if command_not_exists git; then
    sudo apt install -y git
  fi

  sudo apt install -y make automake autoconf libncurses-dev gcc pkg-config

  if command_exists tig; then
    sudo apt purge -y tig
  fi
}

go_redhat() {
  echo -e "Install tig on $(yellow_color "$1")"

  if command_not_exists git; then
    sudo yum install -y git
  fi

  sudo yum install -y make automake autoconf ncurses-devel gcc

  if command_exists tig; then
    sudo yum remove -y tig
  fi
}

distribution="$(get_distribution)"
pretty_name="$(get_pretty_name)"
case "$distribution" in
ubuntu)
  go_ubuntu "$pretty_name"
  ;;
centos | almalinux | rocky)
  go_redhat "$pretty_name"
  ;;
*)
  echo "Not compatible OS"
  ;;
esac

rm -rf /tmp/server-tool/tig
mkdir -p /tmp/server-tool
cd /tmp/server-tool || exit
git clone "https://github.com/jonas/tig"
cd tig || exit

sudo make configure
sudo ./configure
sudo make
sudo make install

installed_block "$(tig -v | head -n 1)"
