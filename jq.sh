#!/bin/bash

base_path="$(dirname "$0")"

. "$base_path/base.sh"

tool="jq"
repo="https://github.com/stedolan/jq"
pretty_name="$(get_pretty_name)"
echo -e "Install $tool on $(yellow_color "$pretty_name")"

rm -rf "/tmp/server-tool/$tool"
mkdir -p /tmp/server-tool/
cd /tmp/server-tool/ || exit
git clone "$repo"
cd "/tmp/server-tool/$tool" || exit

tag=$(git tag --sort=committerdate | tail -n 1)
url="https://github.com/stedolan/jq/releases/download/$tag/jq-linux64"
curl -sL "$url" -o "$tool"
sudo mv "$tool" "/usr/local/bin"
sudo chmod +x "/usr/local/bin/$tool"
installed_block "$($tool --version)"
