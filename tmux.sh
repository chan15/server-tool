#!/bin/bash

base_path="$(dirname "$0")"

. "$base_path/base.sh"

bashrc_config='alias tmux="tmux -2"'
tmux_config=$(
  cat <<EOF
# ~/.tmux.conf
setw -g mode-keys vi

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'dracula/tmux'
set -g @dracula-plugins "git cpu-usage ram-usage time"
set -g @dracula-show-powerline true

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
EOF
)

config_not_in_bashrc() {
  counter=$(cat ~/.bashrc | grep "$bashrc_config" | wc -l)

  if [ "$counter" -gt 0 ]; then
    return 1
  fi

  return 0
}

dracula_not_in_tmux_conf() {
  counter=$(cat ~/.tmux.conf | grep "dracula/tmux" | wc -l)

  if [ "$counter" -gt 0 ]; then
    return 1
  fi

  return 0
}

insert_tmux_conf() {
  echo "$tmux_config" >>~/.tmux.conf
}

dispatch() {
  cd /tmp || exit

  if [ ! -d /tmp/tmux ]; then
    git clone https://github.com/tmux/tmux.git
  fi

  cd tmux || exit
  sudo sh autogen.sh
  sudo ./configure && sudo make
  sudo make install
}

go_ubuntu() {
  echo -e "Install neovim on $(yellow_color "$1")"

  if command_not_exists git; then
    sudo apt install -y git
  fi

  if command_exists tmux; then
    sudo apt purge -y tmux
  fi

  sudo apt install -y git make gcc autoconf automake pkg-config libevent-dev libncurses-dev bison

  dispatch
}

go_redhat() {
  echo -e "Install neovim on $(yellow_color "$1")"

  if command_not_exists git; then
    sudo yum install -y git
  fi

  if command_exists tmux; then
    sudo yum remove -y tmux
  fi

  sudo yum install -y git make gcc autoconf automake pkg-config bison libevent-devel ncurses-devel

  dispatch
}

distribution=$(get_distribution)
pretty_name="$(get_pretty_name)"

case "$distribution" in
ubuntu)
  go_ubuntu "$pretty_name"
  ;;
centos | almalinux | rocky)
  go_redhat "$pretty_name"
  ;;
*)
  echo "Not compatible OS"
  ;;
esac

if [ ! -f ~/.tmux/plugins/tpm ]; then
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi

if [ ! -f ~/.tmux.conf ]; then
  touch ~/.tmux.conf
  insert_tmux_conf
else
  if dracula_not_in_tmux_conf; then
    insert_tmux_conf
  fi
fi

if config_not_in_bashrc; then
  echo -e "\n$bashrc_config" >>~/.bashrc
  source ~/.bashrc
fi

installed_block "$(tmux -V)"
