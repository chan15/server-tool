#!/bin/bash

base_path="$(dirname "$0")"

. "$base_path/base.sh"

tool="jc"
repo="https://github.com/kellyjonbrazil/jc"
pretty_name="$(get_pretty_name)"
echo -e "Install $tool on $(yellow_color "$pretty_name")"

rm -rf "/tmp/server-tool/$tool"
mkdir -p /tmp/server-tool/
cd /tmp/server-tool/ || exit
git clone "$repo"
cd "/tmp/server-tool/$tool" || exit

tag=$(git tag --sort=committerdate | tail -n 1)
version=${tag//v/jc-}
url="https://github.com/kellyjonbrazil/jc/releases/download/$tag/$version-linux-x86_64.tar.gz"
curl -sL -o "$tool.tar.gz" "$url"
mkdir -p "$tool"
tar -zxvf "$tool.tar.gz" -C "$tool"
cd "$tool" || exit
sudo mv "$tool /usr/local/bin"
sudo chmod +x "/usr/local/bin/$tool"

installed_block "$($tool -v | head -n 1)"
