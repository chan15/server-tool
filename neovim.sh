#!/bin/bash

base_path="$(dirname "$0")"

. "$base_path/base.sh"

go_ubuntu() {
  echo -e "Install neovim On $(yellow_color "$1")"

  if command_not_exists git; then
    sudo apt install -y git
  fi
}

go_redhat() {
  echo -e "Install neovim on $(yellow_color "$1")"

  if command_not_exists git; then
    sudo yum install -y git
  fi

  if command_not_exists fuse; then
    sudo yum install -y fuse
  fi
}

distribution=$(get_distribution)
pretty_name="$(get_pretty_name)"

case "$distribution" in
ubuntu)
  go_ubuntu "$pretty_name"
  ;;
centos | rocky | almalinux)
  go_redhat "$pretty_name"
  ;;
*)
  echo "Not compatible OS"
  exit 0
  ;;
esac

mkdir -p /tmp/server-tool/
cd /tmp/server-tool || exit
curl -sLO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
sudo mv nvim.appimage /usr/local/bin/nvim
sudo chmod +x /usr/local/bin/nvim

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
mkdir -p ~/.config/nvim/plugged
curl -sL -o ~/.config/nvim/init.vim https://vim.chan15.info/download/neovim_init_on_linux_by_vim_plug.txt

if nvim +PlugInstall +qall; then
  installed_block "$(nvim -v | head -n 1)"
fi
