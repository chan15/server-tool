RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
END_COLOR="\e[0m"

red_color() {
  echo "${RED}$1${END_COLOR}"
}

green_color() {
  echo "${GREEN}$1${END_COLOR}"
}

yellow_color() {
  echo "${YELLOW}$1${END_COLOR}"
}

blue_color() {
  echo "${BLUE}$1${END_COLOR}"
}

installed_block() {
  echo -e "$(green_color ========================================)"
  echo -e "$(yellow_color "$1") installed"
  echo -e "$(green_color ========================================)"
}

get_distribution() {
  lsb_dist=""
  if [ -r /etc/os-release ]; then
    lsb_dist="$(. /etc/os-release && echo "$ID")"
  fi
  echo "$lsb_dist"
}

get_pretty_name() {
  lsb_dist=""
  if [ -r /etc/os-release ]; then
    lsb_dist="$(. /etc/os-release && echo "$PRETTY_NAME")"
  fi
  echo "$lsb_dist"
}

command_not_exists() {
  command=$1

  if ! command -v "$command" &>/dev/null; then
    return 0
  fi

  return 1
}

command_exists() {
  command=$1

  if command_not_exists "$command"; then
    return 1
  fi

  return 0
}
